import {Injectable} from '@angular/core';
import {LoginModule} from "../login.module";
import {IUserAuth} from "../../../model/IUserAuth";
import {Observable, of} from "rxjs";
import {MOCK_TOKEN} from "../../core/variables/token";

@Injectable()
export class LoginService {

  constructor() {
  }

  login(userAuth: IUserAuth): Observable<string> {
    return of(MOCK_TOKEN);
  }
}
