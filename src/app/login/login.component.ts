import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "../core/providers/auth.service";
import {Router} from "@angular/router";
import {LoginService} from "./providers/login.service";
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject, takeUntil} from "rxjs";

;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  destroy$: Subject<string> = new Subject<string>();
  loginFormGroup: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.pattern('^[\\w_.-]*$')
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      Validators.pattern('^[\\w_.-]*$')
    ])
  })

  constructor(
    private auth: AuthService,
    private router: Router,
    private loginService: LoginService
  ) {
  }

  login(): void {
    this.loginService.login(this.loginFormGroup.value)
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(token => {
        this.auth.setToken(`Bearer ${token}`);
        this.router.navigate(['']);
      })
  }

  getErrorMessage(control: AbstractControl | null | undefined): string {
    if (control) {
      if (control.hasError('required')) {
        return 'Field is required';
      } else if (control.hasError('minlength')) {
        return 'Must be at least 4 characters long'
      } else if (control.hasError('pattern')) {
        return 'Field contains invalid characters'
      }
      return '';
    }
    return '';
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.destroy$.next('');
  }
}
