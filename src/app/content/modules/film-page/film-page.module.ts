import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilmPageRoutingModule } from './film-page-routing.module';
import { FilmPageComponent } from './film-page.component';
import {FilmService} from "../dashboard/providers/film.service";
import {CharacterService} from "../dashboard/providers/character.service";
import {PlanetService} from "../dashboard/providers/planet.service";
import {StarshipService} from "../dashboard/providers/starship.service";
import {MatTableModule} from "@angular/material/table";
import {MatSortModule} from "@angular/material/sort";
import { StarshipDetailDialogComponent } from './components/starship-detail-dialog/starship-detail-dialog.component';
import { CharacterDetailDialogComponent } from './components/character-detail-dialog/character-detail-dialog.component';
import { PlanetDetailDialogComponent } from './components/planet-detail-dialog/planet-detail-dialog.component';
import {MatDialog, MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    FilmPageComponent,
    StarshipDetailDialogComponent,
    CharacterDetailDialogComponent,
    PlanetDetailDialogComponent
  ],
  imports: [
    CommonModule,
    FilmPageRoutingModule,
    MatSortModule,
    MatTableModule,
    MatDialogModule,
    MatButtonModule
  ],
  providers: [
    FilmService,
    CharacterService,
    PlanetService,
    StarshipService
  ]
})
export class FilmPageModule { }
