import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ICharacter} from "../../../dashboard/model/ICharacter";

@Component({
  selector: 'app-character-detail-dialog',
  templateUrl: './character-detail-dialog.component.html',
  styleUrls: ['./character-detail-dialog.component.css']
})
export class CharacterDetailDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public character: ICharacter,
    private dialog: MatDialogRef<CharacterDetailDialogComponent>
  ) {
  }

  close(): void {
    this.dialog.close();
  }

  ngOnInit(): void {
  }

}
