import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {IStarship} from "../../../dashboard/model/IStarship";

@Component({
  selector: 'app-starship-detail-dialog',
  templateUrl: './starship-detail-dialog.component.html',
  styleUrls: ['./starship-detail-dialog.component.css']
})
export class StarshipDetailDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public starship: IStarship,
    private dialog: MatDialogRef<StarshipDetailDialogComponent>
  ) {
  }

  close(): void {
    this.dialog.close();
  }

  ngOnInit(): void {
  }

}
