import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {IPlanet} from "../../../dashboard/model/IPlanet";

@Component({
  selector: 'app-planet-detail-dialog',
  templateUrl: './planet-detail-dialog.component.html',
  styleUrls: ['./planet-detail-dialog.component.css']
})
export class PlanetDetailDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public planet: IPlanet,
    private dialog: MatDialogRef<PlanetDetailDialogComponent>
  ) {
  }

  close(): void {
    this.dialog.close();
  }

  ngOnInit(): void {
  }

}
