import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetDetailDialogComponent } from './planet-detail-dialog.component';

describe('PlanetDetailDialogComponent', () => {
  let component: PlanetDetailDialogComponent;
  let fixture: ComponentFixture<PlanetDetailDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanetDetailDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetDetailDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
