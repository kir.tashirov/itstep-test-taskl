import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {
  combineLatestAll, concatAll, concatMap,
  flatMap, forkJoin,
  map, merge,
  mergeAll,
  mergeMap,
  Observable,
  of, reduce, scan,
  Subject,
  switchMap,
  takeUntil,
  tap, toArray,
  zip
} from "rxjs";
import {IFilm} from "../dashboard/model/IFilm";
import {FilmService} from "../dashboard/providers/film.service";
import {IStarship} from "../dashboard/model/IStarship";
import {ICharacter} from "../dashboard/model/ICharacter";
import {IPlanet} from "../dashboard/model/IPlanet";
import {StarshipService} from "../dashboard/providers/starship.service";
import {CharacterService} from "../dashboard/providers/character.service";
import {PlanetService} from "../dashboard/providers/planet.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {CharacterDetailDialogComponent} from "./components/character-detail-dialog/character-detail-dialog.component";
import {PlanetDetailDialogComponent} from "./components/planet-detail-dialog/planet-detail-dialog.component";
import {StarshipDetailDialogComponent} from "./components/starship-detail-dialog/starship-detail-dialog.component";


@Component({
  selector: 'app-film-page',
  templateUrl: './film-page.component.html',
  styleUrls: ['./film-page.component.css']
})
export class FilmPageComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('starshipsSort', {static: false}) starshipsSort: MatSort;
  @ViewChild('characterSort', {static: false}) characterSort: MatSort;
  @ViewChild('planetSort', {static: false}) planetSort: MatSort;
  film$: Observable<IFilm>;
  destroy$: Subject<string> = new Subject<string>();
  starshipsDisplayedColumns: string[] = ['name', 'starship_class', 'passengers', 'length', 'created'];
  charactersDisplayedColumns: string[] = ['name', 'gender', 'height', 'mass', 'created'];
  planetsDisplayedColumns: string[] = ['name', 'climate', 'gravity', 'population', 'created'];
  starshipsDataSource: MatTableDataSource<IStarship> = new MatTableDataSource<IStarship>();
  planetsDataSource: MatTableDataSource<IPlanet> = new MatTableDataSource<IPlanet>();
  characterDataSource: MatTableDataSource<ICharacter> = new MatTableDataSource<ICharacter>();


  constructor(
    private route: ActivatedRoute,
    private filmService: FilmService,
    private starshipService: StarshipService,
    private characterService: CharacterService,
    private planetService: PlanetService,
    private dialog: MatDialog
  ) {
    this.route.params.pipe(
      takeUntil(this.destroy$)
    )
      .subscribe(params => {
        this.getFilmById(params['id']);
      })
  }

  getFilmById(id: string | number): void {
    this.film$ = this.filmService.getById(id);
  }

  prepareResources(film: IFilm) {
    const starshipList$ = of(film.starships).pipe(
      flatMap(starship => starship),
      mergeMap(starship => this.starshipService.getByUrl(starship)),
      toArray()
    );
    const characterList$ = of(film.characters).pipe(
      flatMap(character => character),
      mergeMap(character => this.characterService.getByUrl(character)),
      toArray()
    );
    const planetList$ = of(film.planets).pipe(
      flatMap(planet => planet),
      mergeMap(planet => this.planetService.getByUrl(planet)),
      toArray()
    )
    starshipList$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(starships => {
      this.starshipsDataSource.data = starships;
      setTimeout(() => {
        this.starshipsDataSource.sort = this.starshipsSort;
      }, 1000)
    })
    characterList$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(characters => {
      this.characterDataSource.data = characters;
      setTimeout(() => {
        this.characterDataSource.sort = this.characterSort;
      }, 1000)
    })
    planetList$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(planets => {
      this.planetsDataSource.data = planets;
      setTimeout(() => {
        this.planetsDataSource.sort = this.planetSort;
      })
    })
  }

  openCharacterDialog(character: ICharacter): void {
    this.dialog.open(CharacterDetailDialogComponent, {data: character, disableClose: true})
  }

  openPlanetDialog(planet: IPlanet): void {
    this.dialog.open(PlanetDetailDialogComponent, {data: planet, disableClose:true})
  }

  openStarshipDialog(starship: IStarship): void {
    this.dialog.open(StarshipDetailDialogComponent, {data: starship, disableClose: true})
  }

  ngOnInit(): void {
    this.film$.subscribe(film => {
      this.prepareResources(film);
    })
  }

  ngAfterViewInit() {
    this.starshipsDataSource.sort = this.starshipsSort;
  }

  ngOnDestroy(): void {
    this.destroy$.next('');
  }
}
