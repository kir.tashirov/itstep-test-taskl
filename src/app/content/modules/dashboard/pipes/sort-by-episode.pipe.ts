import {Pipe, PipeTransform} from '@angular/core';
import {IFilm} from "../model/IFilm";

@Pipe({
  name: 'sortByEpisode'
})
export class SortByEpisodePipe implements PipeTransform {

  transform(films: IFilm[] | null): IFilm[] | null {
    if (!films) {
      return null;
    }
    return films.sort((a, b) => {
      if (a.episode_id > b.episode_id) {
        return 1;
      } else if (a.episode_id < b.episode_id) {
        return -1;
      } else {
        return 0;
      }
    })
  }

}
