import {Pipe, PipeTransform} from '@angular/core';
import {IFilm} from "../model/IFilm";

@Pipe({
  name: 'idFromFilm'
})
export class IdFromFilmPipe implements PipeTransform {

  transform(value: IFilm): string {
    return value.url.replace('https://swapi.dev/api/films', 'film');
  }

}
