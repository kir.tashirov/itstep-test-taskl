import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {IFilm} from "./model/IFilm";
import {FilmService} from "./providers/film.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  films$: Observable<IFilm[]>;

  constructor(private filmService: FilmService) {
  }

  getFilms(): void {
    this.films$ = this.filmService.getList();
  }

  identify(index: number, film: IFilm) {
    return film.url;
  }

  ngOnInit(): void {
    this.getFilms();
  }
}
