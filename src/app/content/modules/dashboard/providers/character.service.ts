import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ICharacter} from "../model/ICharacter";

@Injectable()
export class CharacterService {

  constructor(private http: HttpClient) {
  }

  getByUrl(url: string): Observable<ICharacter> {
    return this.http.get<ICharacter>(url);
  }
}
