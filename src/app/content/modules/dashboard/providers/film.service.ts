import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {IFilm} from "../model/IFilm";

@Injectable()
export class FilmService {

  constructor(private http: HttpClient) {

  }

  getList(): Observable<IFilm[]> {
    return this.http.get<IFilm[]>('https://swapi.dev/api/films')
      .pipe(
        map((response: any) => response.results)
      );
  }

  getByUrl(url: string): Observable<IFilm> {
    return this.http.get<IFilm>(url);
  }

  getById(id: number | string): Observable<IFilm> {
    return this.http.get<IFilm>(`https://swapi.dev/api/films/${id}`);
  }
}
