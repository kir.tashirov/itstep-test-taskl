import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IPlanet} from "../model/IPlanet";
import {Observable} from "rxjs";

@Injectable()
export class PlanetService {

  constructor(private http: HttpClient) {
  }

  getByUrl(url: string): Observable<IPlanet> {
    return this.http.get<IPlanet>(url);
  }
}
