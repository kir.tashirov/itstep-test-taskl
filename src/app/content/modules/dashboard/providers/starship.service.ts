import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IStarship} from "../model/IStarship";

@Injectable()
export class StarshipService {

  constructor(private http: HttpClient) {
  }

  getByUrl(url: string): Observable<IStarship> {
    return this.http.get<IStarship>(url);
  }
}
