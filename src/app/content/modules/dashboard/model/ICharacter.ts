export interface ICharacter{
  name: string;
  gender: string;
  height: number;
  mass: number;
  created: string;
}
