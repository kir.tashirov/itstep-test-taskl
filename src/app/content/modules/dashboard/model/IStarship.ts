export interface IStarship{
  name: string;
  starship_class: string;
  passengers: string;
  length: number;
  created: string;
}
