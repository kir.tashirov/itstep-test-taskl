export interface IPlanet{
  name: string;
  climate: string;
  gravity: string;
  population: number;
  created: string;
}
