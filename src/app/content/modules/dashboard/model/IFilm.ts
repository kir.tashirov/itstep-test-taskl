export interface IFilm{
title: string;
episode_id: number;
director: string;
opening_crawl: string;
starships: string[];
planets: string[];
characters: string[];
created: string;
producer: string;
url: string;
}
