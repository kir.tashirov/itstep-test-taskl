import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { FilmCardComponent } from './components/film-card/film-card.component';
import {MatCardModule} from "@angular/material/card";
import {FilmService} from "./providers/film.service";
import { SortByEpisodePipe } from './pipes/sort-by-episode.pipe';
import { IdFromFilmPipe } from './pipes/id-from-film.pipe';


@NgModule({
  declarations: [
    DashboardComponent,
    FilmCardComponent,
    SortByEpisodePipe,
    IdFromFilmPipe
  ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        MatCardModule
    ],
  providers: [
    FilmService
  ]
})
export class DashboardModule { }
