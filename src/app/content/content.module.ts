import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from './content.component';
import { MainMenuComponent } from './components/main-menu/main-menu.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";


@NgModule({
  declarations: [
    ContentComponent,
    MainMenuComponent,
    ToolbarComponent
  ],
  imports: [
    CommonModule,
    ContentRoutingModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class ContentModule { }
