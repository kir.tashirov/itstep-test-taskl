import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../core/providers/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {
  username: string;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.username = this.authService.getName();
  }

  logout(): void {
    this.authService.clearAll();
    this.router.navigate(['login']);
  }

  ngOnInit(): void {
  }

}
