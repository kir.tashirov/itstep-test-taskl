import {Injectable} from '@angular/core';


@Injectable()
export class AuthService {

  constructor() {
  }

  clearAll(): void {
    localStorage.clear();
  }

  public getToken(): string | null {
    return localStorage.getItem('token');
  }

  public setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  public deleteToken(): void {
    localStorage.removeItem('token');
  }

  public getName(): string {
    return 'Kyle Katarn';
  }

  public getPermissions(): string[] {
    return ['edit', 'delete', 'read', 'teach_jedi'];
  }

}
